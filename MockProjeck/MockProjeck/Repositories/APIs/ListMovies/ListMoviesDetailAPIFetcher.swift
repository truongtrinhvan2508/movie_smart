//
//  ListMoviesDetailAPIFetcherProtocol.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

protocol ListMoviesDetailAPIFetcherProtocol {
    func fetchListDetailMovies(id:Int,result: @escaping (Result<ListMovieDetailResponseEntity, NetworkServiceError>) -> Void)
    func fetchListCastsMovies(id:String,result: @escaping (Result<ListCastsResponseEntity, NetworkServiceError>) -> Void)
    func fetchListSimilarMovies(id:String,result: @escaping (Result<ListSimilarResponseEntity, NetworkServiceError>) -> Void)
}

final class ListMoviesDetailAPIFetcher: BaseAPIFetcher {
    override func apiURL(_ apiParams: String? = nil) -> URL? {
        guard let path = apiParams else {
            return nil
        }
        
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(path)") else {
            return nil
        }
        
        return url
    }
}

extension ListMoviesDetailAPIFetcher: ListMoviesDetailAPIFetcherProtocol {
    func fetchListSimilarMovies(id: String, result: @escaping (Result<ListSimilarResponseEntity, NetworkServiceError>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(id)/similar")  else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }
        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = ListSimilarRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06", page: 1)
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }
            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListSimilarResponseEntity.self)
                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
    
    func fetchListCastsMovies(id: String, result: @escaping (Result<ListCastsResponseEntity, NetworkServiceError>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(id)/credits")  else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }
        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = ListCastsRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06")
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }
            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListCastsResponseEntity.self)
                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
    
    
    func fetchListDetailMovies(id:Int, result: @escaping (Result<ListMovieDetailResponseEntity, NetworkServiceError>) -> Void) {
        
        guard let url = apiURL(String(id)) else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }
        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = ListMovieDetailRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06")
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }
            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListMovieDetailResponseEntity.self)
                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
}
