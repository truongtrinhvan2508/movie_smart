//
//  ListMoviesGenresAPIFetcher.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation

protocol ListMoviesGenresAPIFetcherProtocol{

    func fetchListGenres(result: @escaping (Result<ListGenresResponseEntity, NetworkServiceError>) -> Void)
    func fetchListDetailGenrest(idList: Int, page:Int, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void)
}

final class ListMoviesGenresAPIFetcher: BaseAPIFetcher {
//    override func apiURL() -> URL? {
//
//        guard let url = URL(string: "https://api.themoviedb.org/3/genre/movie/list") else {
//            return nil
//        }
//
//        return url
//    }
}


extension ListMoviesGenresAPIFetcher: ListMoviesGenresAPIFetcherProtocol{
    func fetchListGenres(result: @escaping (Result<ListGenresResponseEntity, NetworkServiceError>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/genre/movie/list") else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }

        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = ListMovieDetailRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06")
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }

            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListGenresResponseEntity.self)
                    
                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
    
    func fetchListDetailGenrest(idList: Int, page: Int, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/discover/movie") else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }

        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = DetailGenresRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06", idGenres: idList, page: page)
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }

            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListMovieResponseEntity.self)
                    
                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }

}
