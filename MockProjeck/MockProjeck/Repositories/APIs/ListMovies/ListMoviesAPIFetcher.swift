//
//  ListMoviesAPIFetcher.swift
//  MVP-sample
//
//  Created by Hoang Minh Nhat on 02/03/2022.
//

import Foundation

protocol ListMoviesAPIFetcherProtocol {
    func fetchListMovies(page:Int, type: String, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void)
    func fetchListSearch(query: String, page: Int, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void)
}

final class ListMoviesAPIFetcher: BaseAPIFetcher {
    override func apiURL(_ apiParams: String? = nil) -> URL? {
        guard let path = apiParams else {
            return nil
        }
        
        guard let url = URL(string: "https://api.themoviedb.org/3/movie/\(path)") else {
            return nil
        }
        
        return url
    }
}

extension ListMoviesAPIFetcher: ListMoviesAPIFetcherProtocol {
    func fetchListMovies(page:Int, type: String, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void) {
        
        guard let url = apiURL(type) else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }

        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = ListMovieRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06", page: page)
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }

            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListMovieResponseEntity.self)
                    

                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
    func fetchListSearch(query: String, page: Int, result: @escaping (Result<ListMovieResponseEntity, NetworkServiceError>) -> Void) {
        guard let url = URL(string: "https://api.themoviedb.org/3/search/movie") else {
            dLogDebug("URL nil")
            result(.failure(.noData))
            return
        }

        let requestInfo = RequestInfo(urlInfo: url, httpMethod: .get)
        // API key get from keychain
        let bodyParams = SearchMovieRequestEntity(apiKey: "d5b97a6fad46348136d87b78895a0c06", query: query, page: page)
        networkService.requestAPI(info: requestInfo, params: bodyParams) { [weak self] dataResult in
            guard let self = self else {
                dLogDebug("PropertySearch API Fetcher nil before complete callback")
                return
            }

            switch dataResult {
            case .success(let data):
                print(data)
                do {
                    let responseEntity = try self.decodeData(data, type: ListMovieResponseEntity.self)
                    

                    result(.success(responseEntity))
                } catch {
                    dLogDebug("Unknow error, return decode failed")
                    result(.failure(.noData))
                }
            case .failure(let networkServicesError):
                print(networkServicesError)
            }
        }
    }
}
