//
//  NetworkServiceProtocols
//  eBankBiz
//
//  Created by GST on 01/12/2021.
//  Copyright (c) 2021 GST. All rights reserved.
//

import Foundation

protocol NetworkServiceProtocol {
    func request(info: RequestInfo, result: @escaping (Result<Data, NetworkServiceError>) -> Void)
    func requestAPI<T: Codable>(info: RequestInfo, params: T, result: @escaping (Result<Data, NetworkServiceError>) -> Void)
}
