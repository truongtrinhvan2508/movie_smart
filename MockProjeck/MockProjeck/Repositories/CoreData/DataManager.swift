//
//  DataManaged.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation
import CoreData
final class DatabaseManager{
    static let shared = DatabaseManager()
    private let modelName = "CoreDataModel"
    private let movieFavoriteEntity = "Favorite"
    private lazy var managedObjectModel: NSManagedObjectModel? = {
        guard let modelUrl = Bundle.main.url(forResource: modelName, withExtension: "momd") else{
            return nil
        }
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl)else {
            return nil
        }
        return managedObjectModel
    }()
    
    private lazy var coordinator: NSPersistentStoreCoordinator = {
        let persistentCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel!)
        var documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let persistentStoreUrl = documentDirectoryUrl.appendingPathComponent("\(modelName).sqlite")
        do{
            let options = [NSMigratePersistentStoresAutomaticallyOption:true, NSInferMappingModelAutomaticallyOption: true]
            try persistentCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreUrl, options: options)
        }catch let error{
            debugPrint("Error: create persistenrCoordinator")
        }
        return persistentCoordinator
    }()
    
    private lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    private func saveContext() -> Bool{
        if !managedObjectContext.hasChanges{
            return true
        }
        do{
            try managedObjectContext.save()
            return true
        }catch let error{
            debugPrint("Error: Save contact: \(error)")
            managedObjectContext.rollback()
            return false
        }
    }
}
extension DatabaseManager{
    func addFavorite(id: Int) -> Bool{
        let entity = NSEntityDescription.insertNewObject(forEntityName: movieFavoriteEntity, into: managedObjectContext) as! Favorite
        entity.id = (Int32)(id)
        return saveContext()
    }
    func checkFavorite(id: Int) -> Bool{
        let fetchRequest = Favorite.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        do{
            if let contact = try managedObjectContext.fetch(fetchRequest).first{
                return true
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        return false
    }
    func deleteFavorite(id: Int){
        let fetchRequest = Favorite.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %d", id)
        do{
            let favoriteList = try managedObjectContext.fetch(fetchRequest)
            for fa in favoriteList{
                managedObjectContext.delete(fa)
            }
        }catch let error{
            debugPrint("Error: \(error)")
        }
        _ = saveContext()
    }
}
