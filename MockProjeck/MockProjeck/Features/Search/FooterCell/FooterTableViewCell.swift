//
//  FooterTableViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 21/03/2022.
//

import UIKit

final class FooterTableViewCell: UITableViewCell {

    @IBOutlet weak var loadMoreIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
