//
//  SearchModel.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

final class SearchModel {
    // Convert to dependencies injection
    private let listSearchServices: ListMovieSearchServices = ListMovieSearchServices()
    private let listMovieDetailServices: ListMoviesDetailServices  = ListMoviesDetailServices()
}

extension SearchModel: SearchModelProtocol {
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListDetailMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListSearchMovies(query: String, page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listSearchServices.getListSearchMovies(query: query, page: page) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
}
