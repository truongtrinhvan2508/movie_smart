//
//  SearchViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 08/03/2022.
//

import UIKit

final class SearchViewController: UIViewController {

    @IBOutlet private weak var searchBarMovie: UISearchBar!
    @IBOutlet private weak var SearchTableView: UITableView!
    
    private var presenter = SearchPresenter(model: SearchModel())
    private var currentQuery = ""
    private var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        setupTableView()
    }


    private func setupTableView() {
        
        searchBarMovie.delegate = self
        SearchTableView.register(UINib(nibName: "SimilarTableViewCell", bundle: nil), forCellReuseIdentifier: "SimilarTableViewCell")
        let tableViewLoadingCellNib = UINib(nibName: "FooterTableViewCell", bundle: nil)
        SearchTableView.register(tableViewLoadingCellNib, forCellReuseIdentifier: "loadingcellid")
        SearchTableView.delegate = self
        SearchTableView.dataSource = self
        SearchTableView.separatorStyle = .none
    }
    
    private func performSearch() {
        guard let query = searchBarMovie.text?.trimmingCharacters(in: .whitespaces),
            query != currentQuery else {
            return
        }
        if query.isEmpty {
            presenter.clearTableView()
            return
        }
        presenter.fetchListSearch(query: query)
        currentQuery = query
    }
    
    private func nextPharse(movieId: Int){
        guard let vc = UIStoryboard(name: "DetailViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else {return}
        vc.movieId = movieId
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
        NotificationCenter.default.post(name: .movieId, object: nil,userInfo: ["movieId": movieId])
    }
}

//MARK: - UITableViewDelegate
extension SearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 400
        } else {
            return 55
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if (offsetY > contentHeight - scrollView.frame.height) && !presenter.isLoading {
            presenter.loadMore(query: currentQuery)
        }
    }
    
}

//MARK: - UITableViewDataSource
extension SearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return presenter.numberOfRowsInSection(section: section)
        } else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SimilarTableViewCell", for: indexPath) as? SimilarTableViewCell else {
                return UITableViewCell()
            }
            let dataCell = presenter.cellForRowAt(indexPath: indexPath)
            let dataCellGenres = presenter.detailForItem(movieId: dataCell.id )
            cell.binDataGenres(data: dataCellGenres ?? ListMovieDetailResponseEntity(genres: [], overview: "", posterPath: "", releaseDate: "", runtime: 0, spokenLanguages: [], productionCountries: [], title: "", voteAverage: 0))
            cell.binData(data: dataCell)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "loadingcellid", for: indexPath) as! FooterTableViewCell
            cell.loadMoreIndicator.startAnimating()
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var movieId: Int = 0
        movieId = presenter.cellForRowAt(indexPath: indexPath).id
        nextPharse(movieId: movieId)
    }
    
    
}

//MARK: - UISearchBarDelegate
extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [weak self] timer in
            // search after 0.5 second
            self?.performSearch()
        })
        return true
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        currentQuery = ""
        presenter.clearTableView()
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

extension SearchViewController: SearchViewProtocol{
    func showErrorMessage(_ message: String) {
        dLogDebug(message)
    }
    
    func refeshView() {
        SearchTableView.reloadData()
    }
    
    
}
