//
//  SearchPresenter.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

final class SearchPresenter {
    
    //MARK: - Properties
    private weak var contentView: SearchContract.View?
    private var presenter = DiscoverPresenter(model: DiscoverModel())
    private var model: SearchContract.Model
    private var searchEntity: [MovieEntity] = []
    private var pageSearch: Int = 1
    private var query: String = ""
    private var listMovieDetailLoaded: [Int: ListMovieDetailResponseEntity] = [:]
    private let dispatchGroup = DispatchGroup()
    var isLoading: Bool = false
    init(model: SearchContract.Model) {
        self.model = model
    }
}

extension SearchPresenter: SearchPresenterProtocol {
    
    
    func clearTableView() {
        self.pageSearch = 1
        searchEntity.removeAll()
        contentView?.refeshView()
    }
    func loadMore(query: String) {
        if !self.isLoading{
            self.isLoading = true
            self.pageSearch += 1
            DispatchQueue.global(qos: .userInitiated).async {
                sleep(2)
                self.fetchListSearch(query: query)
                DispatchQueue.main.async {
                    self.isLoading = false
                }
            }
        }
    }
    func fetchListSearch(query: String) {
        model.fetchListSearchMovies(query: query, page: pageSearch) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.searchEntity.append(contentsOf: entity.results)
                self?.fetchMovieDetail(movies: entity.results)
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchMovieDetail( movies: [MovieEntity]) {
        movies.forEach { movie in
            model.fetchMovieDetail(movieId: movie.id) { [weak self] result in
                switch result {
                case .success(let entity):
                    self?.listMovieDetailLoaded[movie.id] = entity
                    self?.contentView?.refeshView()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func detailForItem(movieId: Int) -> ListMovieDetailResponseEntity? {
        return listMovieDetailLoaded[movieId]
    }
    
    func attach(view: SearchContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return searchEntity.count
    }

    func cellForRowAt(indexPath: IndexPath) -> MovieEntity {
        return searchEntity[indexPath.row]
    }
}
