//
//  SearchContract.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

protocol SearchContract {
    typealias Model = SearchModelProtocol
    typealias View = SearchViewProtocol
    typealias Presenter = SearchPresenterProtocol
}

protocol SearchModelProtocol {
    func fetchListSearchMovies(query: String ,page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
}

protocol SearchViewProtocol: AnyObject {
    func showErrorMessage(_ message: String)
    func refeshView()
}

protocol SearchPresenterProtocol {
    func attach(view: SearchContract.View)
    func detachView()
    func clearTableView()
    func fetchListSearch(query: String)
    func numberOfRowsInSection(section: Int) -> Int
    func cellForRowAt(indexPath: IndexPath) -> MovieEntity
}
