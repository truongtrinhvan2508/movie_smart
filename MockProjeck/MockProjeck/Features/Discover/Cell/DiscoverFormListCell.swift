//
//  DiscoverFormListCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 09/03/2022.
//

import UIKit

class DiscoverFormListCell: UICollectionViewCell {
    private var glColors:[String] = ["gl1","gl2","gl3"]
    private var id: Int = 0
    @IBOutlet private weak var favoriteBtn: UIButton!
    @IBOutlet private weak var backgroudImag: UIImageView!
    @IBOutlet private weak var overviewMovieLb: UILabel!
    @IBOutlet private weak var timePlayMovieLb: UILabel!
    @IBOutlet private weak var titleMovieLb: UILabel!
    @IBOutlet private weak var posterMovieImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setBackGroudImg()
    }
    @IBAction func clickFavorite(_ sender: Any) {
        if !DatabaseManager.shared.checkFavorite(id: id) {
            DatabaseManager.shared.addFavorite(id: id)
        }else {
            DatabaseManager.shared.deleteFavorite(id: id)
        }
        if !DatabaseManager.shared.checkFavorite(id: id) {
            favoriteBtn.tintColor = .systemGray4
        }else {
            favoriteBtn.tintColor = .yellow
        }
        
        NotificationCenter.default.post(name: .favorite, object: nil)

    }
    func bindData(data: MovieEntity) {
        titleMovieLb.text = data.title
        overviewMovieLb.text = data.overview
        DownloadImage.loadImage(posterPath: data.posterpath, imageView: self.posterMovieImage)
        id = data.id
        if !DatabaseManager.shared.checkFavorite(id: id) {
            favoriteBtn.tintColor = .systemGray4
        }else {
            favoriteBtn.tintColor = .yellow
        }
    }
    func setBackGroudImg(){
        if let random = glColors.randomElement() {
            backgroudImag.image = UIImage(named: random)
        }
    }
    func bindDuration(movieDetail: ListMovieDetailResponseEntity?) {
        timePlayMovieLb.text = convertTime(seconds: movieDetail?.runtime ?? 0)
    }
    func convertTime (seconds : Int) -> String {
        let time = "\((seconds % 3600) / 60)h \((seconds % 3600) % 60)m"
        return time
    }
}
