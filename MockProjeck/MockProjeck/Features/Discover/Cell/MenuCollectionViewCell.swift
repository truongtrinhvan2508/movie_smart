//
//  MenuCollectionViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 21/03/2022.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var selectedView: UIView!
    @IBOutlet private weak var titleMenuLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var isSelected: Bool {
        didSet {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3) {
                    self.titleMenuLb.textColor = self.isSelected ? UIColor.black : UIColor.lightGray
                    self.selectedView.backgroundColor = self.isSelected ? .systemTeal : .clear
                    self.layoutIfNeeded()
                }
            }
        }
    }
    func binData(title: String) {
        titleMenuLb.text = title
    }
}
