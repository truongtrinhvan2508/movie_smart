//
//  HeaderMovieCollectionViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 15/03/2022.
//

import UIKit

final class HeaderMovieCollectionViewCell: UICollectionViewCell {
    private var indexSection: Int = 0
    @IBOutlet weak var seeAllBtn: UIButton!
    @IBOutlet weak var titleCategoryLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setTitleCategory(data:String){
        titleCategoryLb.text = data
    }
    @IBAction func clickSeeAllBtn(_ sender: UIButton) {
        let index = sender.tag
        NotificationCenter.default.post(name: .changePage, object: nil,userInfo: ["key": index + 1])
    }
    
}
