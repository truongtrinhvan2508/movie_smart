//
//  FooterMovieCollectionViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 15/03/2022.
//

import UIKit

final class FooterMovieCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var loadMoreIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
