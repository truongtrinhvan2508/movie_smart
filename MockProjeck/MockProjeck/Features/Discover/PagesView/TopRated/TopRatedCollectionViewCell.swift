//
//  TopRatedViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 11/03/2022.
//

import UIKit

final class TopRatedCollectionViewCell: UICollectionViewCell {
    private var presenter = DiscoverPresenter(model: DiscoverModel())
    private var insetsSession = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    private var itemPerRow:CGFloat = 1.0
    private let refreshControl = UIRefreshControl()
    private var loadingView: FooterMovieCollectionViewCell?
    @IBOutlet private weak var TopRatedCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.attach(view: self)
        setupTableView()
        presenter.getDataTopRatedMovies(indPage: 1)
        refresherControl()
        addObserver()
    }
    private func refresherControl(){
        if #available(iOS 10.0, *) {
            self.TopRatedCollectionView.refreshControl = refreshControl
        } else {
            self.TopRatedCollectionView.addSubview(refreshControl)
        }
        
        self.refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        
        self.refreshControl.tintColor = UIColor.lightGray
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Data...", attributes: attributes)
    }
    
    @objc private func updateData() {
        self.refreshControl.beginRefreshing()
        presenter.pullToRefreshTopRated()
        self.TopRatedCollectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
    private func setupTableView() {
        TopRatedCollectionView.register(UINib(nibName: "DiscoverFormListCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverFormListCell")
        TopRatedCollectionView.register(UINib(nibName: "DiscoverFormCollumnsCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverFormCollumnsCell")
        TopRatedCollectionView.register(UINib(nibName: "FooterMovieCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "FooterMovieCollectionViewCell")
        TopRatedCollectionView.dataSource = self
        TopRatedCollectionView.delegate = self
    }
    
    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.favorite), name: .favorite, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeFormList), name: .changeFormList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeFormColumn), name: .changeFormColumn, object: nil)
    }
 
    @objc func changeFormList(){
        itemPerRow = 1.0
        TopRatedCollectionView.reloadData()
    }
    
    @objc func changeFormColumn(){
        itemPerRow = 2.0
        TopRatedCollectionView.reloadData()
    }
    
    @objc func favorite(){
        TopRatedCollectionView.reloadData()
    }
}


extension TopRatedCollectionViewCell: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.numberOfRowsInSectionTopRated(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dataCell = presenter.cellForRowAtTopRated(indexPath: indexPath)
        if itemPerRow == 1{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiscoverFormListCell.self), for: indexPath) as? DiscoverFormListCell else {
                return UICollectionViewCell()
            }
            cell.bindDuration(movieDetail: presenter.detailForItem(movieId: dataCell.id))
            cell.bindData(data: dataCell)
            return cell
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiscoverFormCollumnsCell.self), for: indexPath) as? DiscoverFormCollumnsCell else {
            return UICollectionViewCell()
        }
        cell.bindDuration(movieDetail: presenter.detailForItem(movieId: dataCell.id))
        cell.bindData(data: dataCell)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        presenter.checkLoadMoreTopRated(IndexPathRow: indexPath.row)
    }
}

extension TopRatedCollectionViewCell: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FooterMovieCollectionViewCell", for: indexPath) as! FooterMovieCollectionViewCell
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
            return aFooterView
        }
        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.loadMoreIndicator.startAnimating()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.loadMoreIndicator.stopAnimating()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var indexPathId: Int = 0
        indexPathId = presenter.cellForRowAtTopRated(indexPath: indexPath).id
        NotificationCenter.default.post(name: .nextPhase, object: nil,userInfo: ["nextPhaseMovies": indexPathId])
    }
}

extension TopRatedCollectionViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = CGFloat((itemPerRow + 1)) * insetsSession.left
        let availableWidth = self.contentView.frame.width - paddingSpace
        let width = availableWidth / itemPerRow
        if itemPerRow == 2{
            return CGSize(width: width, height: 250)
        }
        return CGSize(width: width, height: 150)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        insetsSession
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
            return CGSize(width: TopRatedCollectionView.bounds.size.width, height: 55)
    }
    
}

extension TopRatedCollectionViewCell: DiscoverViewProtocol {
    func alertError() {
        dLogDebug("no data")
    }
    
    func refeshView() {
        TopRatedCollectionView.reloadData()
    }
    
    func showErrorMessage(_ message: String) {
        dLogDebug(message)
    }
    
    func displayListMovie(_ listMovie: [MovieEntity]) {
        dLogDebug(listMovie)
    }
    
}
