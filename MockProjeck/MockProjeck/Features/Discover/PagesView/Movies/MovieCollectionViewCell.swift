//
//  MovieCollectionViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 14/03/2022.
//

import UIKit

protocol updateUIParent{
    func updateUI(isLoading: Bool)
}

final class MovieCollectionViewCell: UICollectionViewCell{
    
    var delegate: updateUIParent?
    private var presenter = DiscoverPresenter(model: DiscoverModel())
    private var insetsSession = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    private var itemPerRow:CGFloat = 1.0
    private var listCategory:[String] = ["Popular","Top Rated","Up Coming","Now Playing"]
    var dataMovies: [MovieEntity] = []
    private var formCell = DiscoverFormListCell.self
    var indexPathSection: Int = 0
    @IBOutlet private weak var MovieCollectionView: UICollectionView!
    private let refreshControl = UIRefreshControl()
    override func awakeFromNib() {
        super.awakeFromNib()
        presenter.attach(view: self)
        setupTableView()
        presenter.getDataMovies()
        addObserver()
//        removeObserver()
        refresherControl()
    }
    private func configueDataMovies(data: [MovieEntity]){
        dataMovies = data
        MovieCollectionView.reloadData()
    }
    private func refresherControl(){
        if #available(iOS 10.0, *) {
            self.MovieCollectionView.refreshControl = refreshControl
        } else {
            self.MovieCollectionView.addSubview(refreshControl)
        }
        
        self.refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        
        self.refreshControl.tintColor = UIColor.lightGray
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refreshing Data...", attributes: attributes)
    }
    
    @objc private func updateData() {
//        sleep(2)
//        refreshControl.is
            presenter.pullToRefreshMovies()
            self.MovieCollectionView.reloadData()
            self.refreshControl.endRefreshing()
    }
    
    private func setupTableView() {
        MovieCollectionView.register(UINib(nibName: "DiscoverFormListCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverFormListCell")
        MovieCollectionView.register(UINib(nibName: "DiscoverFormCollumnsCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverFormCollumnsCell")
        MovieCollectionView.register(UINib(nibName: "HeaderMovieCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderMovieCollectionViewCell")
        MovieCollectionView.dataSource = self
        MovieCollectionView.delegate = self
    }
        
    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.favorite), name: .favorite, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeFormList), name: .changeFormList, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changeFormColumn), name: .changeFormColumn, object: nil)
    }
    
    @objc func changeFormList(){
        itemPerRow = 1.0
        MovieCollectionView.reloadData()
    }
    
    @objc func changeFormColumn(){
        itemPerRow = 2.0
        MovieCollectionView.reloadData()
    }
    @objc func favorite(){
        MovieCollectionView.reloadData()
    }
}


extension MovieCollectionViewCell: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        4
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter.numberOfRowsInSectionMovies(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if itemPerRow == 1{
            let dataCell = presenter.cellForRowAtMovies(indexPath: indexPath, indexPathSection: indexPath.section)
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiscoverFormListCell.self), for: indexPath) as? DiscoverFormListCell else {
                return UICollectionViewCell()
            }
            cell.bindDuration(movieDetail: presenter.detailForItem(movieId: dataCell.id))
            cell.bindData(data: dataCell)
            return cell
        }else{
            let dataCell = presenter.cellForRowAtMovies(indexPath: indexPath, indexPathSection: indexPath.section)
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: DiscoverFormCollumnsCell.self), for: indexPath) as? DiscoverFormCollumnsCell else {
                return UICollectionViewCell()
            }
            cell.bindDuration(movieDetail: presenter.detailForItem(movieId: dataCell.id))
            cell.bindData(data: dataCell)
            return cell
        }
        
    }
    
}
extension MovieCollectionViewCell: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let aHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderMovieCollectionViewCell", for: indexPath) as! HeaderMovieCollectionViewCell
            aHeader.setTitleCategory(data: listCategory[indexPath.section])
            aHeader.seeAllBtn.tag = indexPath.section
            return aHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var indexPathId: Int = 0
        indexPathId = presenter.cellForRowAtMovies(indexPath: indexPath, indexPathSection: indexPath.section).id
        NotificationCenter.default.post(name: .nextPhase, object: nil,userInfo: ["nextPhaseMovies": indexPathId])
    }
}

extension MovieCollectionViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = CGFloat((itemPerRow + 1)) * insetsSession.left
        let availableWidth = self.contentView.frame.width - paddingSpace
        let width = availableWidth / itemPerRow
        if itemPerRow == 2{
            return CGSize(width: width, height: 250)
        }
        return CGSize(width: width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        insetsSession
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: MovieCollectionView.bounds.size.width, height: 55)
    }
    
}


extension MovieCollectionViewCell: DiscoverViewProtocol {
    func alertError() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.delegate?.updateUI(isLoading: false)
        }
    }
    
    func refeshView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.delegate?.updateUI(isLoading: true)
        }
        MovieCollectionView.reloadData()
    }
    
    func showErrorMessage(_ message: String) {
        dLogDebug(message)
    }
    
    func displayListMovie(_ listMovie: [MovieEntity]) {
        dLogDebug(listMovie)
    }
    
}

