//
//  DiscoverModel.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 11/03/2022.
//

import Foundation

final class DiscoverModel {
    
    private let listMovieServices: ListMoviesServices = ListMoviesServices()
    private let listMovieDetailServices: ListMoviesDetailServices  = ListMoviesDetailServices()
//    private var movieEntity: [MovieEntity] = []

}

extension DiscoverModel: DiscoverModelProtocol {
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListDetailMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    
    func fetchListTopRatedMovies(indexPage:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMovieServices.getListMovies(page: indexPage, type: "top_rated") { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListUpComingMovies(indexPage:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMovieServices.getListMovies(page: indexPage, type: "upcoming") { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    
    func fetchListPopularMovies(indexPage:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMovieServices.getListMovies(page: indexPage, type: "popular") { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListNowPlayingMovies(indexPage:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMovieServices.getListMovies(page: indexPage, type: "now_playing") { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
}
