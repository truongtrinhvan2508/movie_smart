//
//  DiscoverPresenter.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 09/03/2022.
//

import Foundation

final class DiscoverPresenter {
    private weak var contentView: DiscoverContract.View?
    private var model: DiscoverContract.Model
//    private var modelDetail: DetailContract.Model
    private var dataDetailMovie: ListMovieDetailResponseEntity?
    private var movieEntity: [[MovieEntity]] = []
    private var moviePopularEntity: [MovieEntity] = []
    private var dataPopularEntity: [MovieEntity] = []
    private var movieTopRatedEntity: [MovieEntity] = []
    private var dataTopRatedEntity: [MovieEntity] = []
    private var movieUpComingEntity: [MovieEntity] = []
    private var dataUpComingEntity: [MovieEntity] = []
    private var movieNowPlayingEntity: [MovieEntity] = []
    private var dataNowPlayingEntity: [MovieEntity] = []
    private let QueueConcurrent = DispatchQueue(label: "com.TruongTV18.Smartmovie", attributes: .concurrent)
    private let dispatchGroup = DispatchGroup()
    private var workItemFetchPlaceData: DispatchWorkItem? = nil
    private var isLoadingPopular:Bool = false
    private var isLoadingTopRated:Bool = false
    private var isLoadingUpComing:Bool = false
    private var isLoadingNowPlaying:Bool = false
    private var PagePopular:Int = 1
    private var PageTopRated:Int = 1
    private var PageUpComing:Int = 1
    private var PageNowPlaying:Int = 1
    private var listMovieDetailLoaded: [Int: ListMovieDetailResponseEntity] = [:]
    init(model: DiscoverContract.Model) {
        self.model = model
    }
}

extension DiscoverPresenter{
        
    func pullToRefreshPopular() {
        dataPopularEntity.removeAll()
        getDataPopularMovies(indPage: 1)
    }
    
    func pullToRefreshTopRated() {
        dataTopRatedEntity.removeAll()
        getDataTopRatedMovies(indPage: 1)
    }
    
    func pullToRefreshUpComing() {
        dataUpComingEntity.removeAll()
        getDataUpComingMovies(indPage: 1)
    }
    
    func pullToRefreshNowPlaying() {
        dataNowPlayingEntity.removeAll()
        getDataNowPlayingMovies(indPage: 1)
    }
    
    
    func getDataMovies() {
        workItemFetchPlaceData?.cancel()
        var newWorkItemFetchPlaceData: DispatchWorkItem? = nil
        
        newWorkItemFetchPlaceData = DispatchWorkItem { [weak self] in
            guard let _self = self else { return }
            if newWorkItemFetchPlaceData?.isCancelled == true {
                _self.moviePopularEntity = []
                _self.movieTopRatedEntity = []
                _self.movieUpComingEntity = []
                _self.movieNowPlayingEntity = []
                dLogError("work item cancel!")
                return
            }
            _self.isLoadingPopular = false
            _self.dispatchGroup.enter()
            _self.model.fetchListPopularMovies(indexPage: 1) { result in
                switch result {
                case .success(let entity):
                    self?.moviePopularEntity = entity.results
                    _self.fetchMovieDetail(movies: self?.moviePopularEntity ?? [])
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.moviePopularEntity = []
                    _self.dispatchGroup.leave()
                }
            }
            
            _self.dispatchGroup.enter()
            _self.model.fetchListTopRatedMovies(indexPage: 1){ result in
                switch result {
                case .success(let entity):
                    self?.movieTopRatedEntity = entity.results
                    _self.fetchMovieDetail(movies: self?.movieTopRatedEntity ?? [])
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.movieTopRatedEntity = []
                    _self.dispatchGroup.leave()
                }
            }
            
            _self.dispatchGroup.enter()
            _self.model.fetchListUpComingMovies(indexPage: 1) { result in
                switch result {
                case .success(let entity):
                    self?.movieUpComingEntity = entity.results
                    _self.fetchMovieDetail(movies: self?.movieUpComingEntity ?? [])
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.movieUpComingEntity = []
                    _self.dispatchGroup.leave()
                }
            }
            
            _self.dispatchGroup.enter()
            _self.model.fetchListNowPlayingMovies(indexPage: 1) { result in
                switch result {
                case .success(let entity):
                    _self.movieNowPlayingEntity = entity.results
                    _self.fetchMovieDetail(movies: self?.movieNowPlayingEntity ?? [])
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.movieNowPlayingEntity = []
                    _self.dispatchGroup.leave()
                }
            }
        }
        
        /// notify when work item done
        newWorkItemFetchPlaceData?.notify(queue: QueueConcurrent) {
            if self.dispatchGroup.wait(timeout: .now() + 4) == .timedOut {
                DispatchQueue.main.async(){
                    self.contentView?.alertError()
                    self.contentView?.refeshView()
                }
            } else {
                DispatchQueue.main.async(){
                    self.contentView?.refeshView()
                }
            }
        }
        
        /// execute work item at background
        QueueConcurrent.async(execute: newWorkItemFetchPlaceData!)
        workItemFetchPlaceData = newWorkItemFetchPlaceData
    }
    
    func getDataPopularMovies(indPage: Int) {
        isLoadingPopular = false
        model.fetchListPopularMovies(indexPage: indPage) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.moviePopularEntity = entity.results
                if let moviePopularEntity = self?.moviePopularEntity{
                    for i in 0..<moviePopularEntity.count {
                        self?.dataPopularEntity.append(moviePopularEntity[i])
                    }
                }
                self?.fetchMovieDetail(movies: self?.dataPopularEntity ?? [])
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
                self?.dataPopularEntity = []
                self?.contentView?.alertError()
            }
        }
    }
    
    func getDataTopRatedMovies(indPage: Int) {
        isLoadingTopRated = false
        model.fetchListTopRatedMovies(indexPage: indPage) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.movieTopRatedEntity = entity.results
                if let movieTopRatedEntity = self?.movieTopRatedEntity{
                    for i in 0..<movieTopRatedEntity.count {
                        self?.dataTopRatedEntity.append(movieTopRatedEntity[i])
                    }
                }
                self?.fetchMovieDetail(movies: self?.dataTopRatedEntity ?? [])
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
                self?.dataTopRatedEntity = []
                self?.contentView?.alertError()
            }
        }
    }
    
    func getDataUpComingMovies(indPage: Int) {
        isLoadingUpComing = false
        model.fetchListUpComingMovies(indexPage: indPage) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.movieUpComingEntity = entity.results
                if let movieUpComingEntity = self?.movieUpComingEntity{
                    for i in 0..<movieUpComingEntity.count {
                        self?.dataUpComingEntity.append(movieUpComingEntity[i])
                    }
                }
                self?.fetchMovieDetail(movies: self?.dataUpComingEntity ?? [])
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
                self?.dataUpComingEntity = []
                self?.contentView?.alertError()
            }
        }
    }
    
    func getDataNowPlayingMovies(indPage: Int) {
        isLoadingNowPlaying = false
        model.fetchListNowPlayingMovies(indexPage: indPage) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.movieNowPlayingEntity = entity.results
                if let movieNowPlayingEntity = self?.movieNowPlayingEntity{
                    for i in 0..<movieNowPlayingEntity.count {
                        self?.dataNowPlayingEntity.append(movieNowPlayingEntity[i])
                    }
                }
                self?.fetchMovieDetail(movies: self?.dataNowPlayingEntity ?? [])
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
                self?.dataNowPlayingEntity = []
                self?.contentView?.alertError()
            }
        }
    }
    func fetchMovieDetail( movies: [MovieEntity]) {
        movies.forEach { movie in
            model.fetchMovieDetail(movieId: movie.id) { [weak self] result in
                switch result {
                case .success(let entity):
                    self?.listMovieDetailLoaded[movie.id] = entity
                    self?.contentView?.refeshView()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }

    func detailForItem(movieId: Int) -> ListMovieDetailResponseEntity? {
        return listMovieDetailLoaded[movieId]
    }

    
    func attach(view: DiscoverContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func numberOfSection() -> Int{
        4
    }
    
    func numberOfRowsInSectionMovies(section: Int) -> Int {
        
        switch section{
        case 0:
            if moviePopularEntity.count <= 4 {
                return moviePopularEntity.count
            }
            return 4
        case 1:
            if movieTopRatedEntity.count <= 4 {
                return movieTopRatedEntity.count
            }
            return 4
        case 2:
            if movieUpComingEntity.count <= 4 {
                return movieUpComingEntity.count
            }
            return 4
        default:
            if movieNowPlayingEntity.count <= 4 {
                return movieNowPlayingEntity.count
            }
            return 4
        }
    }
    
    func cellForRowAtMovies(indexPath: IndexPath, indexPathSection: Int) -> MovieEntity {
        switch indexPathSection{
        case 0:
            return moviePopularEntity[indexPath.row]
            
        case 1:
            return movieTopRatedEntity[indexPath.row]
        case 2:
            return movieUpComingEntity[indexPath.row]
        default:
            return movieNowPlayingEntity[indexPath.row]
        }
    }
    
    func pullToRefreshMovies() {
        moviePopularEntity.removeAll()
        movieTopRatedEntity.removeAll()
        movieUpComingEntity.removeAll()
        movieNowPlayingEntity.removeAll()
        getDataMovies()
    }
    
    func numberOfRowsInSectionPopular(section: Int) -> Int {
        return dataPopularEntity.count
    }
    
    func cellForRowAtPopular(indexPath: IndexPath) -> MovieEntity {
        return dataPopularEntity[indexPath.row]
    }
    
    func numberOfRowsInSectionTopRated(section: Int) -> Int {
        return dataTopRatedEntity.count
    }
    
    func cellForRowAtTopRated(indexPath: IndexPath) -> MovieEntity {
        return dataTopRatedEntity[indexPath.row]
    }
    
    func numberOfRowsInSectionUpComing(section: Int) -> Int {
        return dataUpComingEntity.count
    }
    
    func cellForRowAtUpComing(indexPath: IndexPath) -> MovieEntity {
        return dataUpComingEntity[indexPath.row]
    }
    
    func numberOfRowsInSectionNowPlaying(section: Int) -> Int {
        return dataNowPlayingEntity.count
    }
    
    func cellForRowAtNowPlaying(indexPath: IndexPath) -> MovieEntity {
        return dataNowPlayingEntity[indexPath.row]
    }
    
    func loadMorePopular() {
        if !self.isLoadingPopular {
            self.isLoadingPopular = true
            self.PagePopular += 1
            DispatchQueue.global(qos: .userInitiated).async {
                sleep(2)
                self.getDataPopularMovies(indPage: self.PagePopular)
                DispatchQueue.main.async {
                    self.isLoadingPopular = false
                }
            }
        }
        
    }
    
    func loadMoreTopRated() {
        if !self.isLoadingTopRated {
            self.isLoadingTopRated = true
            self.PageTopRated += 1
            DispatchQueue.global(qos: .userInitiated).async {
                sleep(2)
                self.getDataTopRatedMovies(indPage: self.PageTopRated)
                DispatchQueue.main.async {
                    self.isLoadingTopRated = false
                }
            }
        }
    }
    
    func loadMoreUpComing() {
        if !self.isLoadingUpComing {
            self.isLoadingUpComing = true
            self.PageUpComing += 1
            DispatchQueue.global(qos: .userInitiated).async {
                sleep(2)
                self.getDataUpComingMovies(indPage: self.PageUpComing)
                DispatchQueue.main.async {
                    self.isLoadingUpComing = false
                }
            }
        }
    }
    
    func loadMoreNowPlaying() {
        if !self.isLoadingNowPlaying {
            self.isLoadingNowPlaying = true
            self.PageNowPlaying += 1
            DispatchQueue.global(qos: .userInitiated).async {
                sleep(2)
                self.getDataNowPlayingMovies(indPage: self.PageNowPlaying)
                DispatchQueue.main.async {
                    self.isLoadingNowPlaying = false
                }
            }
        }
    }
    
    func checkLoadMorePopular(IndexPathRow: Int) {
        if IndexPathRow == dataPopularEntity.count - 4 && !self.isLoadingPopular{
            loadMorePopular()
        }
    }
    
    func checkLoadMoreTopRated(IndexPathRow: Int) {
        if IndexPathRow == dataTopRatedEntity.count - 4 && !self.isLoadingTopRated{
            loadMoreTopRated()
        }
    }
    
    func checkLoadMoreUpComing(IndexPathRow: Int) {
        if IndexPathRow == dataUpComingEntity.count - 4 && !self.isLoadingUpComing{
            loadMoreUpComing()
        }
    }
    
    func checkLoadMoreNowPlaying(IndexPathRow: Int) {
        if IndexPathRow == dataNowPlayingEntity.count - 4 && !self.isLoadingNowPlaying{
            loadMoreNowPlaying()
        }
    }
    
    func sizeLoadMorePopular(size: Int) -> (sizeWidth: Int, sizeHeight: Int) {
        if self.isLoadingPopular {
            return (sizeWidth: 0, sizeHeight: 0)
        } else {
            return (sizeWidth: size, sizeHeight: 55)
        }
        
    }
    
    func sizeLoadMoreTopRated(size: Int) -> (sizeWidth: Int, sizeHeight: Int) {
        if self.isLoadingTopRated {
            return (sizeWidth: 0, sizeHeight: 0)
        } else {
            return (sizeWidth: size, sizeHeight: 55)
        }
    }
    
    func sizeLoadMoreUpComing(size: Int) -> (sizeWidth: Int, sizeHeight: Int) {
        if self.isLoadingUpComing {
            return (sizeWidth: 0, sizeHeight: 0)
        } else {
            return (sizeWidth: size, sizeHeight: 55)
        }
    }
    
    func sizeLoadMoreNowPlaying(size: Int) -> (sizeWidth: Int, sizeHeight: Int) {
        if self.isLoadingNowPlaying {
            return (sizeWidth: 0, sizeHeight: 0)
        } else {
            return (sizeWidth: size, sizeHeight: 55)
        }
    }
}
