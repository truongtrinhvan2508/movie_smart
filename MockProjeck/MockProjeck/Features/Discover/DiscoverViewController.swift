//
//  DiscoverViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 08/03/2022.
//

import UIKit

final class DiscoverViewController: UIViewController{
    
    
    
    // MARK: - Variables
    private var listMenuDiscover:[String] = ["Movies","Popular","Top Rated","Up Coming","Now Playing"]
    private var presenterDetail = DetailPresenter(model: DetailModel())
    private var presenter = DiscoverPresenter(model: DiscoverModel())
    private var check: Bool = false
    private var insetsSession = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    private var itemPerRow:CGFloat = 1.0
    private var indexPathSection: Int = 0
    private var detailMovie: ListMovieDetailResponseEntity?
    // MARK: ActivityIndicatorView
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    
    //MARK: Button
    @IBOutlet private weak var checkStyleBtn: UIButton!
    
    
    @IBOutlet private weak var MenuCollectionView: UICollectionView!
    @IBOutlet private weak var DiscoverCollectionView: UICollectionView!
    
    //MARK: viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
        setUpCollectionView()
        MenuCollectionView.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
    }
    override func viewWillAppear(_ animated: Bool){
        DiscoverCollectionView.isUserInteractionEnabled = true
        addObserver()
    }
    override func viewDidDisappear(_ animated: Bool) {
        removeObserver()
    }
    

    private func addObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.changePage(notification:)), name: .changePage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.nextPharse(notification:)), name: .nextPhase, object: nil)
        
    }
    private func formatRunTime(_ time: Int) -> String {
        var hour: Int = 0
        var minute: Int = 0
        hour = Int(time/60)
        minute = time - hour * 60
        return "\(hour)h, \(minute)mins"
        
    }
    private func removeObserver(){
        NotificationCenter.default.removeObserver(self, name: .changePage, object: nil)
        NotificationCenter.default.removeObserver(self, name: .nextPhase, object: nil)
    }
    
    @objc func changePage(notification: Notification){
        if let indexPage = notification.userInfo?["key"] as? Int {
            scrollToIndex(index: indexPage)
            MenuCollectionView.selectItem(at: IndexPath(item: indexPage, section: 0), animated: true, scrollPosition: .centeredHorizontally)
            MenuCollectionView.scrollToItem(at: IndexPath(item: indexPage, section: 0), at: .centeredHorizontally, animated: true)
            
        }
    }
    
    @objc func nextPharse(notification: Notification){
        if let movieId = notification.userInfo?["nextPhaseMovies"] as? Int {
            nextPharse(movieId: movieId)
        }
    }
    
    
    
    private func nextPharse(movieId: Int){
        guard let vc = UIStoryboard(name: "DetailViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else {return}
        vc.movieId = movieId
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
        NotificationCenter.default.post(name: .movieId, object: nil,userInfo: ["movieId": movieId])
    }
    
    private func setUpCollectionView(){
        MenuCollectionView.delegate = self
        MenuCollectionView.dataSource = self
        DiscoverCollectionView.delegate = self
        DiscoverCollectionView.dataSource = self
        MenuCollectionView.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCollectionViewCell")
        DiscoverCollectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MovieCollectionViewCell")
        DiscoverCollectionView.register(UINib(nibName: "PopularCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PopularCollectionViewCell")
        DiscoverCollectionView.register(UINib(nibName: "TopRatedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TopRatedCollectionViewCell")
        DiscoverCollectionView.register(UINib(nibName: "UpComingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "UpComingCollectionViewCell")
        DiscoverCollectionView.register(UINib(nibName: "NowPlayingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NowPlayingCollectionViewCell")
    }
    
    //MARK: change style
    @IBAction func clickChangeStyleCollectionView(_ sender: UIButton) {
        if check == true{
            checkStyleBtn.setBackgroundImage(UIImage(named: "Category"), for: .normal)
            NotificationCenter.default.post(name: .changeFormList, object: nil)
            check = false
        }else{
            checkStyleBtn.setBackgroundImage(UIImage(named: "Discover"), for: .normal)
            NotificationCenter.default.post(name: .changeFormColumn, object: nil)
            check = true
        }
        
    }
    private func scrollToIndex(index: Int){
        DiscoverCollectionView.scrollToItem(at:IndexPath(item: 0, section: index), at: .centeredVertically, animated: true)
//        DiscoverCollectionView.reloadData()
        MenuCollectionView.selectItem(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        MenuCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: true)
//        MenuCollectionView.reloadData()
    }

}

extension DiscoverViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == MenuCollectionView{
            scrollToIndex(index: indexPath.row)
        }else{
            
        }
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        for cell in DiscoverCollectionView.visibleCells {
//            let indexPath = DiscoverCollectionView.indexPath(for: cell)
////            MenuCollectionView.selectItem(at: IndexPath(item: indexPath?.section ?? 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
////            MenuCollectionView.scrollToItem(at: IndexPath(item: indexPath?.section ?? 0, section: 0), at: .centeredHorizontally, animated: true)
//////            MenuCollectionView.reloadData()
////            print(indexPath?.section ?? 0)
//            scrollToIndex(index: indexPath?.section ?? 0)
//        }
//    }


}

extension DiscoverViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == MenuCollectionView{
            return 1
        }
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == MenuCollectionView{
            return listMenuDiscover.count
        }
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == MenuCollectionView{
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MenuCollectionViewCell.self), for: indexPath) as? MenuCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.binData(title: listMenuDiscover[indexPath.row])
            return cell
        }
        switch indexPath.section{
        case 0:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: MovieCollectionViewCell.self), for: indexPath) as? MovieCollectionViewCell else {
                return UICollectionViewCell()
            }
            cell.delegate = self
            return cell
        case 1:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PopularCollectionViewCell.self), for: indexPath) as? PopularCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        case 2:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TopRatedCollectionViewCell.self), for: indexPath) as? TopRatedCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        case 3:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: UpComingCollectionViewCell.self), for: indexPath) as? UpComingCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        default:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: NowPlayingCollectionViewCell.self), for: indexPath) as? NowPlayingCollectionViewCell else {
                return UICollectionViewCell()
            }
            return cell
        }
    }

}

extension DiscoverViewController:UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == MenuCollectionView{
            itemPerRow = 3.7
        }else{
            itemPerRow = 1.0
        }
        let paddingSpace = CGFloat((itemPerRow + 1)) * insetsSession.left
        let availableWidth = DiscoverCollectionView.frame.width - paddingSpace
        let width = availableWidth / itemPerRow
        let height = DiscoverCollectionView.frame.height - paddingSpace
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        insetsSession
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
}

extension DiscoverViewController: updateUIParent{
    func updateUI(isLoading: Bool) {
        if isLoading == false{
            let alert = UIAlertController(title: "Load data failed", message: "Can'n get data from server, please try again later", preferredStyle: .alert)
            let alertActionReload = UIAlertAction(title: "Reload", style: .default) { (act) in
            }
            alert.addAction(alertActionReload)
            self.present(alert, animated: true, completion: nil)
        }else{
            loadingIndicator.isHidden = true
        }
    }
}


