//
//  DiscoverContract.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 09/03/2022.
//

import Foundation

protocol DiscoverContract {
    typealias Model = DiscoverModelProtocol
    typealias View = DiscoverViewProtocol
//    typealias Presenter = DiscoverPresenterProtocol
}
protocol DiscoverModelProtocol {
    func fetchListPopularMovies(indexPage: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchListTopRatedMovies(indexPage: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchListUpComingMovies(indexPage: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchListNowPlayingMovies(indexPage: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
}
protocol DiscoverViewProtocol: AnyObject {
    func showErrorMessage(_ message: String)
    func displayListMovie(_ listMovie: [MovieEntity])
    func refeshView()
    func alertError()
}

//protocol DiscoverPresenterProtocol {
//    func attach(view: DiscoverContract.View)
//    func detachView()
//    func getDataMovies()
//    func getDataPopularMovies(indPage: Int)
//    func getDataTopRatedMovies(indPage: Int)
//    func getDataUpComingMovies(indPage: Int)
//    func getDataNowPlayingMovies(indPage: Int)
//    func numberOfRowsInSectionMovies(section: Int) -> Int
//    func cellForRowAtMovies(indexPath: IndexPath, indexPathSection: Int) -> MovieEntity
//    func numberOfRowsInSectionPopular(section: Int) -> Int
//    func cellForRowAtPopular(indexPath: IndexPath) -> MovieEntity
//    func numberOfRowsInSectionTopRated(section: Int) -> Int
//    func cellForRowAtTopRated(indexPath: IndexPath) -> MovieEntity
//    func numberOfRowsInSectionUpComing(section: Int) -> Int
//    func cellForRowAtUpComing(indexPath: IndexPath) -> MovieEntity
//    func numberOfRowsInSectionNowPlaying(section: Int) -> Int
//    func cellForRowAtNowPlaying(indexPath: IndexPath) -> MovieEntity
//    func loadMorePopular()
//    func loadMoreTopRated()
//    func loadMoreUpComing()
//    func loadMoreNowPlaying()
//    func checkLoadMorePopular(IndexPathRow: Int)
//    func checkLoadMoreTopRated(IndexPathRow: Int)
//    func checkLoadMoreUpComing(IndexPathRow: Int)
//    func checkLoadMoreNowPlaying(IndexPathRow: Int)
//    func pullToRefreshMovies()
//    func pullToRefreshPopular()
//    func pullToRefreshTopRated()
//    func pullToRefreshUpComing()
//    func pullToRefreshNowPlaying()
////    func sizeLoadMorePopular(size: Int) -> (sizeWidth: Int, sizeHeight: Int)
////    func sizeLoadMoreTopRated(size: Int) -> (sizeWidth: Int, sizeHeight: Int)
////    func sizeLoadMoreUpComing(size: Int) -> (sizeWidth: Int, sizeHeight: Int)
////    func sizeLoadMoreNowPlaying(size: Int) -> (sizeWidth: Int, sizeHeight: Int)
//}
