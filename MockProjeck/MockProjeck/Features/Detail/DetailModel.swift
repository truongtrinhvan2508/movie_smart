//
//  DetailModel.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 16/03/2022.
//

import Foundation

final class DetailModel {
    
    private let listMovieDetailServices: ListMoviesDetailServices = ListMoviesDetailServices()
    
//    private var movieEntity: [MovieEntity] = []

}

extension DetailModel: DetailModelProtocol {
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListDetailMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListSimilarMovies(movieId: String, result: @escaping (Result<ListSimilarResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListSimilarMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListCastsMovies(movieId: String, result: @escaping (Result<ListCastsResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListCastsMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    
    func fetchListDetailMovies(movieId : Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListDetailMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
}
