//
//  DetailContract.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 16/03/2022.
//

import Foundation

protocol DetailContract {
    typealias Model = DetailModelProtocol
    typealias View = DetailViewProtocol
}

protocol DetailViewProtocol: AnyObject {
    func showErrorMessage(_ message: String)
    func refeshView()
    func alertError()
}


protocol DetailModelProtocol {
    func fetchListDetailMovies(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
    func fetchListCastsMovies(movieId: String, result: @escaping (Result<ListCastsResponseEntity, Error>) -> Void)
    func fetchListSimilarMovies(movieId: String, result: @escaping (Result<ListSimilarResponseEntity, Error>) -> Void)
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
}
