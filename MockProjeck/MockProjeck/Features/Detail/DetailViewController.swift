//
//  DetailViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 15/03/2022.
//

import UIKit

final class DetailViewController: UIViewController {
    
    private var presenter = DetailPresenter(model: DetailModel())
    private var dataCasts: [Casts] = []
    private var dataSimilar: [ListSimilarEntity] = []
    private var dataSimilarGenres: ListMovieDetailResponseEntity = ListMovieDetailResponseEntity(genres: [], overview: "", posterPath: "", releaseDate: "", runtime: 0, spokenLanguages: [], productionCountries: [], title: "", voteAverage: 0)
    @IBOutlet private weak var loadingToDetail: UIActivityIndicatorView!
    @IBOutlet private weak var ratingStarImg: UIImageView!
    @IBOutlet private weak var ratingLb: UILabel!
    @IBOutlet private weak var releaseDateLb: UILabel!
    @IBOutlet private weak var languageMovieLb: UILabel!
    @IBOutlet private weak var genresMovieLb: UILabel!
    @IBOutlet private weak var titleMovieLb: UILabel!
    @IBOutlet private weak var profileMovieImg: UIImageView!
    @IBOutlet private weak var DetailTableView: UITableView!
    @IBOutlet private weak var overViewLb: UILabel!
    var movieId : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadingToDetail.isHidden = false
        loadingToDetail.startAnimating()
        presenter.getDataDetailMovies(movieId: movieId, view: self)
        setupTableView()
    }
    
    
    @IBAction func clickBackBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func convertRuntime (time : Int) -> String {
        let time = "\((time % 3600) / 60)h \((time % 3600) % 60)m"
        return time
    }
    
    private func setupTableView(){
        
        DetailTableView.delegate = self
        DetailTableView.dataSource = self
        DetailTableView.register(UINib(nibName: "CastsAndCrewTableViewCell", bundle: nil), forCellReuseIdentifier: "CastsAndCrewTableViewCell")
        DetailTableView.register(UINib(nibName: "SimilarTableViewCell", bundle: nil), forCellReuseIdentifier: "SimilarTableViewCell")
        
    }
    
}

extension DetailViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return dataSimilar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1{
            guard let cellSimilar = tableView.dequeueReusableCell(withIdentifier: "SimilarTableViewCell", for: indexPath) as? SimilarTableViewCell else {
                return UITableViewCell()
            }
            let dataCell = dataSimilar[indexPath.row]
            cellSimilar.binDataGenres(data: presenter.detailForItem(movieId: dataCell.id) ?? ListMovieDetailResponseEntity(genres: [], overview: "", posterPath: "", releaseDate: "", runtime: 0, spokenLanguages: [], productionCountries: [], title: "", voteAverage: 0))
            cellSimilar.configueData(data: dataCell)
            return cellSimilar
        }
        guard let cellCasts = tableView.dequeueReusableCell(withIdentifier: "CastsAndCrewTableViewCell", for: indexPath) as? CastsAndCrewTableViewCell else {
            return UITableViewCell()
        }
        cellCasts.configueData(data: dataCasts)
        return cellCasts
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            return 400
        }
        return 400
    }
    
}

extension DetailViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let  headerCell = tableView.dequeueReusableCell(withIdentifier: "HeaderDetailTableViewCell") as? HeaderDetailTableViewCell else {return UITableViewCell()}
        switch (section){
        case 0:
            headerCell.titleHeaderDetail.text = "Casts"
        default:
            headerCell.titleHeaderDetail.text = "Similar"
        }

        
        return headerCell
    }
}



extension DetailViewController: updateUIProtocol{
    func updateUI(dataResponse: ListMovieDetailResponseEntity, dataResponseCasts: [Casts], dataResponseSimilar: [ListSimilarEntity]) {
        DispatchQueue.main.async {
            
            //MARK: dataDetail
            self.titleMovieLb.text = dataResponse.title
            let time = self.convertRuntime(time: dataResponse.runtime)
//            let coutries = dataResponse.productionCountries[0].iso31661
            let releaseDate = dataResponse.releaseDate
            self.releaseDateLb.text = "\(releaseDate)  \(time)"            
            DownloadImage.loadImage(posterPath: dataResponse.posterPath, imageView: self.profileMovieImg)
            self.overViewLb.text = dataResponse.overview
            self.ratingLb.text = "\(String(dataResponse.voteAverage))/10"
            var genres: String = ""
            for genreIndex in 0..<dataResponse.genres.count {
                if genreIndex == dataResponse.genres.count - 1 {
                    genres.append("\(dataResponse.genres[genreIndex].name)")
                } else {
                    genres.append("\(dataResponse.genres[genreIndex].name) | ")
                }
            }
            self.genresMovieLb.text = genres
            var languageMovie: String = ""
            for langugeIndex in 0..<dataResponse.spokenLanguages.count {
                if langugeIndex == dataResponse.spokenLanguages.count - 1{
                    languageMovie.append("\(dataResponse.spokenLanguages[langugeIndex].name)")
                }else{
                    languageMovie.append("\(dataResponse.spokenLanguages[langugeIndex].name) | ")
                }
            }
            self.languageMovieLb.text = "Language:\(languageMovie)"
            self.ratingStarImg.image = UIImage(named: self.presenter.checkstar(rating: dataResponse.voteAverage))
            
            //MARK: dataCasts
            self.dataCasts = dataResponseCasts
            
            
            //MARK: dataSimilar
            self.dataSimilarGenres = dataResponse
            self.dataSimilar = dataResponseSimilar
            self.loadingToDetail.isHidden = true
            self.DetailTableView.reloadData()
        }
        
    }
    
    
    
    
}


