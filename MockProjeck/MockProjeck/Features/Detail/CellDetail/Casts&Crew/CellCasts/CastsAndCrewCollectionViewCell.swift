//
//  CastsAndCrewCollectionViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import UIKit

final class CastsAndCrewCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var castsNameLb: UILabel!
    @IBOutlet private weak var castsImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func ConfigueData(data: Casts ){
        castsNameLb.text = data.name
        DownloadImage.loadImage(posterPath: data.profilePath ?? "", imageView: self.castsImg)

    }

}
