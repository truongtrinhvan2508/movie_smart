//
//  CastsAndCrewTableViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import UIKit
import SwiftUI

final class CastsAndCrewTableViewCell: UITableViewCell {
    private var insetsSession = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    private var itemPerRow:CGFloat = 2.0
    private var movieIdcasts: Int = 0
    private var DataCasts:[Casts] = []
    @IBOutlet private weak var castsAndCrewCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code\
//        setupCollectionView()
    }
    func setupCollectionView(){
        castsAndCrewCollectionView.delegate = self
        castsAndCrewCollectionView.dataSource = self
        castsAndCrewCollectionView.register(UINib(nibName: "CastsAndCrewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CastsAndCrewCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configueData(data: [Casts]){
        setupCollectionView()
        DataCasts = data
        castsAndCrewCollectionView.reloadData()
    }

}

extension CastsAndCrewTableViewCell: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataCasts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CastsAndCrewCollectionViewCell", for: indexPath) as? CastsAndCrewCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.ConfigueData(data: DataCasts[indexPath.row])
        return cell
    }
    
    
}

extension CastsAndCrewTableViewCell: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = CGFloat((itemPerRow + 1)) * insetsSession.left
        let availableWidth = self.contentView.frame.width - paddingSpace
        let availableHeight = self.contentView.frame.height - paddingSpace
        let height = availableHeight / itemPerRow
        let width = availableWidth / 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        insetsSession
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
}

