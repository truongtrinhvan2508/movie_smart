//
//  HeaderDetailTableViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import UIKit

final class HeaderDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var titleHeaderDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
