//
//  SimilarTableViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import UIKit

final class SimilarTableViewCell: UITableViewCell {
    private var presenter = DetailPresenter(model: DetailModel())
    @IBOutlet private weak var genresLb: UILabel!
    @IBOutlet private weak var titleLb: UILabel!
    @IBOutlet private weak var ratingImg: UIImageView!
    @IBOutlet private weak var posterImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func binData(data: MovieEntity){
        titleLb.text  = data.title
        DownloadImage.loadImage(posterPath: data.posterpath, imageView: posterImg)
        ratingImg.image = UIImage(named: presenter.checkstar(rating: data.voteaverage))
    }
    func configueData(data: ListSimilarEntity){
        titleLb.text  = data.title
        DownloadImage.loadImage(posterPath: data.posterPath, imageView: posterImg)
        ratingImg.image = UIImage(named: presenter.checkstar(rating: data.voteAverage))
    }
    func binDataGenres(data: ListMovieDetailResponseEntity){
        var genres: String = ""
        for genresIndex in 0..<data.genres.count {
            if genresIndex == data.genres.count - 1{
                genres.append("\(data.genres[genresIndex].name)")
            }else{
                genres.append("\(data.genres[genresIndex].name) | ")
            }
        }
        genresLb.text = genres
    }
}
