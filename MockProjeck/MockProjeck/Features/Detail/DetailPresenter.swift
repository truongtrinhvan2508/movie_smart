//
//  DetailPresenter.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 16/03/2022.
//

import Foundation
import UIKit



protocol updateUIProtocol{
    func updateUI(dataResponse: ListMovieDetailResponseEntity,dataResponseCasts: [Casts],dataResponseSimilar: [ListSimilarEntity])
}

final class DetailPresenter{
    var delegate : updateUIProtocol?
    private weak var contentView: DetailContract.View?
    private var dataDetailMovie: ListMovieDetailResponseEntity?
    private var dataCastsMovie: [Casts] = []
    private var dataSimilarMovie: [ListSimilarEntity] = []
    private var model: DetailContract.Model
    private var listMovieDetailLoaded: [Int: ListMovieDetailResponseEntity] = [:]
    init(model: DetailContract.Model) {
        self.model = model
    }
    private var workItemFetchPlaceData: DispatchWorkItem? = nil
    private let QueueConcurrent = DispatchQueue(label: "com.TruongTV18.Smartmovie", attributes: .concurrent)
    private let dispatchGroup = DispatchGroup()
}

extension DetailPresenter{

    func getDataDetailMovies(movieId: Int,view: UIViewController){
        self.delegate = view as? updateUIProtocol
        workItemFetchPlaceData?.cancel()
        var newWorkItemFetchPlaceData: DispatchWorkItem? = nil
        
        newWorkItemFetchPlaceData = DispatchWorkItem { [weak self] in
            guard let _self = self else { return }
            if newWorkItemFetchPlaceData?.isCancelled == true {
                _self.dataDetailMovie = ListMovieDetailResponseEntity(genres: [], overview: "", posterPath: "", releaseDate: "", runtime: 0, spokenLanguages: [], productionCountries: [], title: "", voteAverage: 0)
                _self.dataCastsMovie = []
                dLogError("work item cancel!")
                return
            }
            _self.dispatchGroup.enter()
            _self.model.fetchListCastsMovies(movieId: String(movieId)) { result in
                switch result {
                case .success(let entity):
                    _self.dataCastsMovie = entity.cast
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.dispatchGroup.leave()
                }
            }
            
            _self.dispatchGroup.enter()
            _self.model.fetchListDetailMovies(movieId: movieId) { result in
                switch result {
                case .success(let entity):
                    _self.dataDetailMovie = entity
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.dispatchGroup.leave()
                }
            }
            
            _self.dispatchGroup.enter()
            _self.model.fetchListSimilarMovies(movieId: String(movieId)) { result in
                switch result {
                case .success(let entity):
                    _self.dataSimilarMovie = entity.results
                    _self.fetchMovieDetail(movies: entity.results)
                    _self.dispatchGroup.leave()
                case .failure(let error):
                    print(error)
                    _self.dispatchGroup.leave()
                }
            }
            
        }
        
        /// notify when work item done
        newWorkItemFetchPlaceData?.notify(queue: QueueConcurrent) {
            self.dispatchGroup.wait()
            
            self.delegate?.updateUI(dataResponse: self.dataDetailMovie ?? ListMovieDetailResponseEntity(genres: [], overview: "", posterPath: "", releaseDate: "", runtime: 0, spokenLanguages: [], productionCountries: [], title: "", voteAverage: 0), dataResponseCasts: self.dataCastsMovie, dataResponseSimilar: self.dataSimilarMovie)
        }
        
        /// execute work item at background
        QueueConcurrent.async(execute: newWorkItemFetchPlaceData!)
        workItemFetchPlaceData = newWorkItemFetchPlaceData
        
    }
    
    func fetchMovieDetail( movies: [ListSimilarEntity]) {
        movies.forEach { movie in
            model.fetchMovieDetail(movieId: movie.id) { [weak self] result in
                switch result {
                case .success(let entity):
                    self?.listMovieDetailLoaded[movie.id] = entity
                    self?.contentView?.refeshView()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
    func detailForItem(movieId: Int) -> ListMovieDetailResponseEntity? {
        return listMovieDetailLoaded[movieId]
    }
    
    func checkstar(rating: Double) -> String {
        switch rating {
        case 10: return "5star"
        case 8..<10: return "4star"
        case 6..<8: return "3star"
        case 4..<6: return "2star"
        default: return"1star"
        }
    }
}
