//
//  TabbarViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 08/03/2022.
//

import UIKit

final class TabbarViewController: UITabBarController {
    let kBarHeight:CGFloat = 180
    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers = [makeNavigationDiscovery(),makeNavigationSearch(),makeNavigationGenres(),makeNavigationArtists()]
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tabBar.backgroundColor = .white
        tabBar.frame.size.height = kBarHeight
        tabBar.frame.origin.y = view.frame.height - kBarHeight
        UITabBar.appearance().tintColor = UIColor(red: 6.0, green: 221.0, blue: 227.0, alpha: 1)
    }
    
    private func makeNavigationDiscovery() -> UINavigationController {
        let vc = UIStoryboard(name: "DiscoverViewController", bundle: nil).instantiateViewController(withIdentifier: "DiscoverViewController") as! DiscoverViewController
        vc.tabBarItem = UITabBarItem(title: "Discover", image: UIImage(named: "Discover"), tag: 1)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationSearch() -> UINavigationController {
        let vc = UIStoryboard(name: "SearchViewController", bundle: nil).instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        vc.tabBarItem = UITabBarItem(title: "Search", image: UIImage(named: "Search"), tag: 2)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationGenres() -> UINavigationController {
        let vc = UIStoryboard(name: "GenresViewController", bundle: nil).instantiateViewController(withIdentifier: "GenresViewController") as! GenresViewController
        vc.tabBarItem = UITabBarItem(title: "Genres", image: UIImage(named: "Genres"), tag: 3)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    private func makeNavigationArtists() -> UINavigationController {
        let vc = UIStoryboard(name: "ArtistsViewController", bundle: nil).instantiateViewController(withIdentifier: "ArtistsViewController") as! ArtistsViewController
        vc.tabBarItem = UITabBarItem(title: "Artists", image: UIImage(named: "Artists"), tag: 4)
        let navi = UINavigationController(rootViewController: vc)
        navi.navigationBar.isHidden = true
        return navi
    }
    
 
}
