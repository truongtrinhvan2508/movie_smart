//
//  DetailGenresViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import UIKit

final class DetailGenresViewController: UIViewController {
    private var insetsSession = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    @IBOutlet private weak var loadingtoDetailGenre: UIActivityIndicatorView!
    @IBOutlet private weak var titleGenreLb: UILabel!
    private var itemPerRow:CGFloat = 1.0
    @IBOutlet private weak var DetailGenreCollectionView: UICollectionView!
    //MARK: - Properties
    private var presenter = GenresPresenter(model: GenresModel())
    var idGenres: Int = 0
    var titleGenre: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        presenter.fetchListDetailGenres(idGenres: idGenres, page: 1)
        titleGenreLb.text = titleGenre
        loadingtoDetailGenre.isHidden = false
        loadingtoDetailGenre.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupCollectionView()
    }

    
    private func setupCollectionView() {
        DetailGenreCollectionView.register(UINib(nibName: "DiscoverFormListCell", bundle: nil), forCellWithReuseIdentifier: "DiscoverFormListCell")
        DetailGenreCollectionView.delegate = self
        DetailGenreCollectionView.dataSource = self
    }
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - UITableViewDelegate
extension DetailGenresViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = CGFloat((itemPerRow + 1)) * insetsSession.left
        let availableWidth = view.frame.width - paddingSpace
        let width = availableWidth / itemPerRow
        return CGSize(width: width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        insetsSession
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        insetsSession.left
    }
    
}

extension DetailGenresViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRowsDetailGenresInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dataCell = presenter.cellForRowDetailGenresAt(indexPath: indexPath)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscoverFormListCell", for: indexPath) as? DiscoverFormListCell else {
            return UICollectionViewCell()
        }
        cell.bindDuration(movieDetail: presenter.detailForItem(movieId: dataCell.id))
        cell.bindData(data: dataCell)
        return cell
    }

}

extension DetailGenresViewController: GenresViewProtocol{
    func showErrorMessage(_ message: String) {
        dLogDebug(message)
    }
    
    func refeshView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.loadingtoDetailGenre.isHidden = true
        }
        DetailGenreCollectionView.reloadData()
    }
    
    
}
