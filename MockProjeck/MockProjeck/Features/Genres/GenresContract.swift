//
//  GenresContract.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation

protocol GenresContract {
    typealias Model = GenresModelProtocol
    typealias View = GenresViewProtocol
    typealias Presenter = GenresPresenterProtocol
}

protocol GenresModelProtocol {
    func fetchListGenresMovies(result: @escaping (Result<ListGenresResponseEntity, Error>) -> Void)
    func fetchListDetailGenresMovie(idList: Int, page:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
}

protocol GenresViewProtocol: AnyObject {
    func showErrorMessage(_ message: String)
    func refeshView()
}

protocol GenresPresenterProtocol {
    func attach(view: GenresContract.View)
    func detachView()
    func fetchListGenres()
    func fetchListDetailGenres(idGenres: Int, page: Int)
    func numberOfRowsInSection(section: Int) -> Int
    func cellForRowAt(indexPath: IndexPath) -> Genre
    
    func numberOfRowsDetailGenresInSection(section: Int) -> Int
    func cellForRowDetailGenresAt(indexPath: IndexPath) -> MovieEntity
}
