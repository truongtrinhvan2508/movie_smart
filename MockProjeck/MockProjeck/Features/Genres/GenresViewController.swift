//
//  GenresViewController.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 08/03/2022.
//

import UIKit

final class GenresViewController: UIViewController {

    @IBOutlet private weak var loadingtoGenres: UIActivityIndicatorView!
    @IBOutlet private weak var genreTableView: UITableView!
    //MARK: - Properties
    private var presenter = GenresPresenter(model: GenresModel())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attach(view: self)
        presenter.fetchListGenres()
        loadingtoGenres.isHidden = false
        loadingtoGenres.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupTableView()
    }
    
    
    private func setupTableView() {
        genreTableView.register(UINib(nibName: "GenresTableViewCell", bundle: nil), forCellReuseIdentifier: "GenresTableViewCell")
        genreTableView.delegate = self
        genreTableView.dataSource = self
    }

}

//MARK: - UITableViewDelegate
extension GenresViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}

//MARK: - UITableViewDataSource
extension GenresViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GenresTableViewCell", for: indexPath) as? GenresTableViewCell else { return UITableViewCell()}
        cell.titleLb.text = presenter.cellForRowAt(indexPath: indexPath).name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "DetailGenresViewController", bundle: nil).instantiateViewController(withIdentifier: "DetailGenresViewController") as? DetailGenresViewController else {return}
        vc.idGenres = presenter.cellForRowAt(indexPath: indexPath).id
        vc.titleGenre = presenter.cellForRowAt(indexPath: indexPath).name
        vc.modalPresentationStyle = .overFullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension GenresViewController: GenresViewProtocol{
    func showErrorMessage(_ message: String) {
        dLogDebug(message)
    }
    
    func refeshView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
            self.loadingtoGenres.isHidden = true
        }
        genreTableView.reloadData()
    }
    
    
}
