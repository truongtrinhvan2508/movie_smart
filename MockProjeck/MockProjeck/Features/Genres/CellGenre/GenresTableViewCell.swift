//
//  GenresTableViewCell.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import UIKit

class GenresTableViewCell: UITableViewCell {

    @IBOutlet weak var genresImg: UIImageView!
    @IBOutlet weak var titleLb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
