//
//  GenresModel.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation

final class GenresModel {
    // Convert to dependencies injection
    private let listGenresServices: ListGenresServices = ListGenresServices()
    private let listMovieDetailServices: ListMoviesDetailServices  = ListMoviesDetailServices()
}

extension GenresModel: GenresModelProtocol {
    
    func fetchMovieDetail(movieId: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailServices.getListDetailMovies(id: movieId) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListGenresMovies(result: @escaping (Result<ListGenresResponseEntity, Error>) -> Void) {
        listGenresServices.getListGenresMovies() { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func fetchListDetailGenresMovie(idList: Int, page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listGenresServices.getListDetailGenresMovie(idList: idList, page: page) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case . failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
}
