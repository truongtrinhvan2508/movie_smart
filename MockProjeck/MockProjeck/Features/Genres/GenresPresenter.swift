//
//  GenresPresenter.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation

final class GenresPresenter {
    private weak var contentView: GenresContract.View?
    private var model: GenresContract.Model
    private var genresEntity: [Genre] = []
    private var detailGenresMovieEntity: [MovieEntity] = []
    private var detailPage: Int = 1
    private var id: Int = 0
    private var listMovieDetailLoaded: [Int: ListMovieDetailResponseEntity] = [:]
    private let dispatchGroup = DispatchGroup()
    init(model: GenresContract.Model) {
        self.model = model
    }
}

extension GenresPresenter: GenresPresenterProtocol {

    func fetchListGenres() {
        model.fetchListGenresMovies() { [weak self] result in
            switch result {
            case .success(let entity):
                self?.genresEntity = entity.genres
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func fetchListDetailGenres(idGenres: Int, page: Int) {
        model.fetchListDetailGenresMovie(idList: idGenres, page: page) { [weak self] result in
            switch result {
            case .success(let entity):
                self?.detailGenresMovieEntity = entity.results
                self?.fetchMovieDetail(movies: self?.detailGenresMovieEntity ?? [])
                self?.contentView?.refeshView()
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func fetchMovieDetail( movies: [MovieEntity]) {
        movies.forEach { movie in
            model.fetchMovieDetail(movieId: movie.id) { [weak self] result in
                switch result {
                case .success(let entity):
                    self?.listMovieDetailLoaded[movie.id] = entity
                    self?.contentView?.refeshView()
                case .failure(let error):
                    print(error)
                }
            }
        }
    }

    func detailForItem(movieId: Int) -> ListMovieDetailResponseEntity? {
        return listMovieDetailLoaded[movieId]
    }
    
    func attach(view: GenresContract.View) {
        contentView = view
    }
    
    func detachView() {
        contentView = nil
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return genresEntity.count
    }

    func cellForRowAt(indexPath: IndexPath) -> Genre {
        return genresEntity[indexPath.row]
    }
    
    func numberOfRowsDetailGenresInSection(section: Int) -> Int {
        detailGenresMovieEntity.count
    }
    
    func cellForRowDetailGenresAt(indexPath: IndexPath) -> MovieEntity {
        return detailGenresMovieEntity[indexPath.row]
    }

}
