//
//  ListMovieGenresServices.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 18/03/2022.
//

import Foundation

protocol ListGenresServicesProtocol {
    func getListGenresMovies(result: @escaping (Result<ListGenresResponseEntity, Error>) -> Void)
    func getListDetailGenresMovie(idList: Int, page:Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
}

final class ListGenresServices {
    // POP Protocol oriented
    private let listMoviesGenresAPIFetcher: ListMoviesGenresAPIFetcherProtocol!
    
    // Dependency injection
    init(_ apiFetcher: ListMoviesGenresAPIFetcherProtocol = ListMoviesGenresAPIFetcher()) {
        listMoviesGenresAPIFetcher = apiFetcher
    }
}

extension ListGenresServices: ListGenresServicesProtocol {
    
    func getListGenresMovies(result: @escaping (Result<ListGenresResponseEntity, Error>) -> Void) {
        listMoviesGenresAPIFetcher.fetchListGenres() { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func getListDetailGenresMovie(idList: Int, page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMoviesGenresAPIFetcher.fetchListDetailGenrest(idList: idList, page: page) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
}
