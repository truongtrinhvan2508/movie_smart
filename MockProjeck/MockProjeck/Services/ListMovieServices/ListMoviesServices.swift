//
//  ListMoviesServices.swift
//  MVP-sample
//
//  Created by Hoang Minh Nhat on 01/03/2022.
//

import Foundation

protocol ListMoviesServicesProtocol {
    func getListMovies(page:Int, type:String, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
}

final class ListMoviesServices {
    private let listMovieAPIFetcher: ListMoviesAPIFetcherProtocol!
    var moviePopularEntity: [MovieEntity] = []
    init(_ apiFetcher: ListMoviesAPIFetcherProtocol = ListMoviesAPIFetcher()) {
        listMovieAPIFetcher = apiFetcher
    }
}

extension ListMoviesServices: ListMoviesServicesProtocol {
    func getListMovies(page:Int, type:String, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void){
//        #warning("TODO - parameters must prepared here")
        
        listMovieAPIFetcher.fetchListMovies(page:page, type: type) {  response in
            switch response {
                // Call back
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
}
