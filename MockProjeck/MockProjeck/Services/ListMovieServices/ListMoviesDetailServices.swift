//
//  ListMoviesDetailServices.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

protocol ListMoviesDetailServicesProtocol {
    func getListDetailMovies(id:Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void)
    func getListCastsMovies(id:String, result: @escaping (Result<ListCastsResponseEntity, Error>) -> Void)
    func getListSimilarMovies(id:String, result: @escaping (Result<ListSimilarResponseEntity, Error>) -> Void)

}

final class ListMoviesDetailServices {
    private let listMovieDetailAPIFetcher: ListMoviesDetailAPIFetcherProtocol!
    init(_ apiFetcher: ListMoviesDetailAPIFetcherProtocol = ListMoviesDetailAPIFetcher()) {
        listMovieDetailAPIFetcher = apiFetcher
    }
}

extension ListMoviesDetailServices: ListMoviesDetailServicesProtocol {
    func getListSimilarMovies(id: String, result: @escaping (Result<ListSimilarResponseEntity, Error>) -> Void) {
        listMovieDetailAPIFetcher.fetchListSimilarMovies(id: id) { response in
            switch response {
                // Call back
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func getListCastsMovies(id: String, result: @escaping (Result<ListCastsResponseEntity, Error>) -> Void) {
        listMovieDetailAPIFetcher.fetchListCastsMovies(id: id) { response in
            switch response {
                // Call back
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
    
    func getListDetailMovies(id: Int, result: @escaping (Result<ListMovieDetailResponseEntity, Error>) -> Void) {
        listMovieDetailAPIFetcher.fetchListDetailMovies(id: id) {  response in
            switch response {
                // Call back
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
}
