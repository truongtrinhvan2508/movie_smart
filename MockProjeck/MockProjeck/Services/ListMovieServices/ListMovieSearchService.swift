//
//  ListMovieSearchService.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation



protocol ListSearchServicesProtocol {
    func getListSearchMovies(query: String, page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void)
}

final class ListMovieSearchServices {
    // POP Protocol oriented
    private let listMovieAPIFetcher: ListMoviesAPIFetcherProtocol!
    
    // Dependency injection
    init(_ apiFetcher: ListMoviesAPIFetcherProtocol = ListMoviesAPIFetcher()) {
        listMovieAPIFetcher = apiFetcher
    }
}

extension ListMovieSearchServices: ListSearchServicesProtocol {
    func getListSearchMovies(query: String, page: Int, result: @escaping (Result<ListMovieResponseEntity, Error>) -> Void) {
        listMovieAPIFetcher.fetchListSearch(query: query, page: page ) { response in
            switch response {
            case .success(let entity):
                dLogDebug(entity)
                result(.success(entity))
            case .failure(let error):
                dLogDebug(error)
                result(.failure(error))
            }
        }
    }
}
