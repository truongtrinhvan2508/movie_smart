//
//  ListMovieRequestEntity.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 09/03/2022.
//

import Foundation


struct ListMovieRequestEntity: Codable {
    let apiKey: String
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case page
    }
}
struct ListMovieResponseEntity: Codable {
    let page: Int
    let results: [MovieEntity]
}

struct MovieEntity: Codable {
    let id: Int
    let title: String
    let overview: String
    var genreIDS: [Int]?
    let voteaverage: Double
    let posterpath: String
//    let releasedate: String
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case overview
        case genreIDS = "genre_ids"
        case voteaverage = "vote_average"
        case posterpath = "poster_path"
//        case releasedate = "release_date"
    }
}
