//
//  ListSearchMovieRequestEntity.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 17/03/2022.
//

import Foundation

struct SearchMovieRequestEntity: Codable {
    let apiKey: String
    let query: String
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case query
        case page
    }
    
}

