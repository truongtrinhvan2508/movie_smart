//
//  File.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 16/03/2022.
//

import Foundation

//MARK: ListMovieDetailRequestEntity
struct ListMovieDetailRequestEntity: Codable {
    let apiKey: String
//    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
//        case page
    }
}

//MARK: ListMovieDetailResponseEntity
struct ListMovieDetailResponseEntity: Codable {

    let genres: [Genre]
    let overview: String
    let posterPath: String
    let releaseDate: String
    let runtime: Int
    let spokenLanguages: [SpokenLanguage]
    let productionCountries: [coutries]
    let title: String
    let voteAverage: Double

    private enum CodingKeys: String, CodingKey {

        case genres
        case overview
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case runtime
        case productionCountries = "production_countries"
        case spokenLanguages = "spoken_languages"
        case title = "title"
        case voteAverage = "vote_average"
        
    }

}

struct coutries: Codable{
    let iso31661 : String
    private enum CodingKeys: String, CodingKey {
        case iso31661 = "iso_3166_1"
    }
}

// MARK: - Genre

struct ListGenresResponseEntity: Codable {
    let genres: [Genre]
    
    private enum CodingKeys: String, CodingKey {
        case genres = "genres"
    }
}

struct Genre: Codable {

    let id: Int
    let name: String
    
    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"

    }
}

struct DetailGenresRequestEntity: Codable {
    let apiKey: String
    let idGenres: Int
    let page: Int
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case idGenres = "with_genres"
        case page
    }
}

// MARK: - SpokenLanguage

struct SpokenLanguage: Codable {

    let name: String

    enum CodingKeys: String, CodingKey {

        case name

    }

}



// MARK: - Similar

struct ListSimilarRequestEntity: Codable{
    let apiKey: String
    let page : Int
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case page
    }
}

struct ListSimilarResponseEntity : Codable{
    
    let results: [ListSimilarEntity]
    
    private enum CodingKeys: String, CodingKey {
        case results
    }
}

struct ListSimilarEntity : Codable{
    let id: Int
    let genresIds : [Int]
    let posterPath : String
    let title : String
    let voteAverage : Double

    private enum CodingKeys: String, CodingKey {
        case id
        case genresIds = "genre_ids"
        case posterPath = "poster_path"
        case title
        case voteAverage = "vote_average"
    }
}

// MARK: - casts
struct ListCastsRequestEntity: Codable{
    let apiKey: String
    
    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
    }
}
struct ListCastsResponseEntity  : Codable{
    let cast : [Casts]
    
    private enum CodingKeys: String, CodingKey {
        case cast

    }
}

struct Casts : Codable{
    let name : String
    let profilePath: String?
    
    private enum CodingKeys: String, CodingKey {
        case name
        case profilePath = "profile_path"
    }

}


