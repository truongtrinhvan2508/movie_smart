//
//  AppDelegate.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 08/03/2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        UIApplication.shared.endIgnoringInteractionEvents()
        window = UIWindow.init(frame: UIScreen.main.bounds)
        let tabbarController = UIStoryboard(name: "TabbarViewController", bundle: nil).instantiateViewController(withIdentifier: "TabbarViewController") as! TabbarViewController
        window?.rootViewController = tabbarController
        window?.makeKeyAndVisible()
        return true
    }
    
    
    
}

