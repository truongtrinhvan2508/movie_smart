//
//  Extension.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 15/03/2022.
//

import Foundation

extension Notification.Name{
    static let changeFormList = Notification.Name("changeFormList")
    static let changeFormColumn = Notification.Name("changeFormColumn")
    static let changePage = Notification.Name("changePage")
    static let nextPhase = Notification.Name("nextPhase")
    static let key = Notification.Name("Key")
    static let movieId = Notification.Name("movieId")
    static let favorite = Notification.Name("favorite")
}
