//
//  LoadImage.swift
//  MockProjeck
//
//  Created by Trịnh Trường on 14/03/2022.
//

import Foundation
import Kingfisher
import UIKit

class DownloadImage{
    static func loadImage(posterPath: String, imageView: UIImageView) {
        if let url = URL(string: "https://image.tmdb.org/t/p/w500\(posterPath)") {
            let processor = DownsamplingImageProcessor(size: imageView.bounds.size)
            |> RoundCornerImageProcessor(cornerRadius: 6)
            imageView.kf.setImage(with: url,
                                  placeholder: nil,
                                  options: [
                                    .processor(processor),
                                    .scaleFactor(UIScreen.main.scale),
                                    .transition(.fade(1)),
                                    .cacheOriginalImage
                                  ])
        }
    }
}
